﻿using System.Runtime.Serialization;
using System;

namespace TransitClassesLibrary.Classes
{
    [DataContract]
    public class WhatsNewObject
    {
        [DataMember(Name = "id", Order = 0)]
        public int ID { set; get; }
        [DataMember(Name = "verssion", Order = 1)]
        public string Verssion { set; get; }
        [DataMember(Name = "news", Order = 2)]
        public byte[] News { set; get; }
        [DataMember(Name = "date", Order = 3)]
        public DateTime Date { set; get; }
        [DataMember(Name = "editinfo", Order = 4)]
        public string EditInfo { set; get; }
    }
}
