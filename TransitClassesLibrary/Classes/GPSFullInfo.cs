﻿using System.Runtime.Serialization;

namespace TransitClassesLibrary.Classes
{
    [DataContract]
    public class GPSFullInfo
    {
        public GPSFullInfo()
        {
        }
        [DataMember(Name = "accuracy", Order = 0)]
        public float Accuracy { get; set; }
        [DataMember(Name = "altitude", Order = 1)]
        public double Altitude { get; set; }
        [DataMember(Name = "bearing", Order = 2)]
        public float Bearing { get; set; }
        [DataMember(Name = "bearingaccuracydegrees", Order = 3)]
        public float BearingAccuracyDegrees { get; set; }
        [DataMember(Name = "latitude", Order = 4)]
        public double Latitude { get; set; }
        [DataMember(Name = "longitude", Order = 5)]
        public double Longitude { get; set; }
        [DataMember(Name = "provider", Order = 6)]
        public string Provider { get; set; }
        [DataMember(Name = "speed", Order = 7)]
        public float Speed { get; set; }
        [DataMember(Name = "time", Order = 8)]
        public long Time { get; set; }
        [DataMember(Name = "verticalaccuracymeters", Order = 9)]
        public float VerticalAccuracyMeters { get; set; }
        [DataMember(Name = "datetime", Order = 10)]
        public string DateTime { get; set; }
    }
}
