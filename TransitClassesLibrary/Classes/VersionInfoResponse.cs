﻿using System.Runtime.Serialization;
using System;

namespace TransitClassesLibrary.Classes
{
    [DataContract]
    public class VersionInfoResponse
    {
        [DataMember(Order = 0)]
        public string City { set; get; }
        [DataMember(Order = 1)]
        public string Version { set; get; }
        [DataMember(Order = 2)]
        public long Length { set; get; }
        [DataMember(Order = 3)]
        public DateTime DateCreated { set; get; }
        [DataMember(Order = 4)]
        public bool IsUpdateEnabled { set; get; }
    }
}
