﻿using System.Runtime.Serialization;

namespace TransitClassesLibrary.Classes
{
    [DataContract]
    public class Tester
    {
        [DataMember(Name = "userid", Order = 0)]
        public string UserID { get; set; }
        [DataMember(Name = "status", Order = 1)]
        public int Status { get; set; }
    }
}
