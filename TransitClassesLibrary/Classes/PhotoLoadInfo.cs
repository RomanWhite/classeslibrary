﻿using System;
using System.Runtime.Serialization;

namespace TransitClassesLibrary.Classes
{

    [DataContract]
    public class PhotoLoadInfo
    {
        [DataMember(Name = "status", Order = 0)]
        public string Status { set; get; }
        [DataMember(Name = "info", Order = 1)]
        public string Info { set; get; }
    }
}
