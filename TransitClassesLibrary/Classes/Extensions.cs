﻿using System.Collections.Generic;
namespace TransitClassesLibrary.Classes
{
    public static class StringExtensions
    {
        public static byte[] GetByteArray(this string s)
        {
            List<byte> List = new List<byte>();
            foreach (var item in s)
            {
                List.Add((byte)(item / byte.MaxValue));
                List.Add((byte)(item % byte.MaxValue));
            }
            return List.ToArray();
        }
    }
    public static class ByteArrayExtensions
    {
        public static string GetString(this byte[] a)
        {
            string Result = "";
            for (int i = 0; i < a.Length; i += 2)
                Result += (char)(a[i] * byte.MaxValue + a[i + 1]);
            return Result;
        }
    }
}
