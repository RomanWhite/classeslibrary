﻿using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TransitClassesLibrary.Classes
{
    [DataContract]
    public class CoordinateRequest
    {
        public CoordinateRequest() { }
        [DataMember(Name = "dataarray", Order = 0)]
        public GPSFullInfo[] DataArray { get; set; }
        [DataMember(Name = "result", Order = 1)]
        public bool Result { get; set; }
        [DataMember(Name = "userid", Order = 2)]
        public string UserId { get; set; }
        [DataMember(Name = "starttime", Order = 3)]
        public string StartTime { get; set; }
        [DataMember(Name = "endtime", Order = 4)]
        public string EndTime { get; set; }
        public void AddData(GPSFullInfo info, string UserID)
        {
            List<GPSFullInfo> temp = new List<GPSFullInfo>(DataArray ?? new GPSFullInfo[0]);
            temp.Add(info);
            DataArray = temp.ToArray();
            UserId = UserID;
        }
        public void Complite(bool result, string EndTimeString)
        {
            EndTime = EndTimeString;
            Result = result;
        }
    }
}
