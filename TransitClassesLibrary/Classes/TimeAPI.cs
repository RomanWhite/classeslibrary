﻿using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.Net;
using System.IO;
using System;

namespace TransitClassesLibrary.Classes
{
    [DataContract]
    public class TimeAPI
    {
        [DataMember(Name = "abbreviation", Order = 0)]
        public string Title { set; get; }
        [DataMember(Name = "utc_datetime", Order = 1)]
        public string datetime { set; get; }
        [DataMember(Name = "timezone", Order = 2)]
        public string TimeZone { set; get; }
        public DateTime GetDateTime() => DateTime.Parse(datetime.Split('T')[0]) + TimeSpan.Parse(datetime.Split('T')[1].Substring(0, 12));
        public static DateTime GetGMTDateTime(int TimeZone = +0)
        {
            using (var wc = new WebClient())
            {
                string TimeJson = wc.DownloadString(@"http://worldtimeapi.org/api/timezone/Etc/GMT");
                using (MemoryStream MemStr = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(TimeJson)))
                    return (new DataContractJsonSerializer(typeof(TimeAPI)).ReadObject(MemStr) as TimeAPI).GetDateTime().AddHours(TimeZone);
            }
        }
        public const int Brn = +7;
    }
}
