﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System;

namespace TransitClassesLibrary.Classes
{
    [DataContract]
    public class NamedPhoto
    {
        [DataMember(Name = "username", Order = 0)]
        public string UserName { get; set; }
        [DataMember(Name = "city", Order = 1)]
        public string City { get; set; }
        [DataMember(Name = "data", Order = 2)]
        public byte[] Data { get; set; }
        public NamedPhoto(string username, string city, byte[] data)
        {
            UserName = username;
            City = city;
            Data = data;
        }
        #region Serialize
        public NamedPhoto(byte[] Value)
        {
            if (Value.Length > 16)
            {
                byte[] Hash = Value.Skip(Value.Length - 16).ToArray();
                Value = Value.Take(Value.Length - 16).ToArray();
                byte[] Hash2 = GetHash(Value);
                if (!Enumerable.SequenceEqual(Hash, Hash2))
                    return;
                int StartIndex = 0;
                int Length = 0;
                //UserName
                {
                    Length = Value[StartIndex++];
                    UserName = Value.Skip(StartIndex).Take(Length).ToArray().GetString();
                    StartIndex += Length;
                }
                //City
                {
                    Length = Value[StartIndex++];
                    City = Value.Skip(StartIndex).Take(Length).ToArray().GetString();
                    StartIndex += Length;
                }
                //ByteValue
                {
                    int LengthLength = Value[StartIndex++];
                    Length = BitConverter.ToInt32(Value.Skip(StartIndex).Take(LengthLength).ToArray(), 0);
                    StartIndex += LengthLength;
                    Data = Value.Skip(StartIndex).Take(Length).ToArray();
                    StartIndex += Length;
                }
            }
        }
        public byte[] ToByteArray()
        {
            List<byte> Result = new List<byte>();
            //UserName
            {
                byte[] TempArray = UserName.GetByteArray();
                Result.Add((byte)TempArray.Length);
                Result.AddRange(TempArray);
            }
            //UserName
            {
                byte[] TempArray = City.GetByteArray();
                Result.Add((byte)TempArray.Length);
                Result.AddRange(TempArray);
            }
            //ByteValue
            {
                byte[] TempLengthArray = BitConverter.GetBytes(Data.Length);
                Result.Add((byte)TempLengthArray.Length);
                Result.AddRange(TempLengthArray);
                Result.AddRange(Data);
            }
            Result.AddRange(GetHash(Result.ToArray()));
            return Result.ToArray();
        }
        #region HelpToSerialize
        byte[] GetHash(byte[] Array)
        {
            byte[] Hash = new byte[16];
            for (int MainIndex = 0; MainIndex < Array.Length / 16; MainIndex++)
                for (int Index = 0; Index < 16; Index++)
                    Hash[Index] = (byte)(((int)Hash[Index] + (int)Array[MainIndex * 16 + Index]) % byte.MaxValue);
            return Hash;
        }
        #endregion
        #endregion
    }
}
