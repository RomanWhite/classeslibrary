﻿using System.Runtime.Serialization;
using System;

namespace TransitClassesLibrary.Classes
{
    [DataContract]
    public class UserVersionView
    {
        [DataMember(Name = "name", Order = 0)]
        public string Name { set; get; }
        [DataMember(Name = "version", Order = 1)]
        public string Version { set; get; }
        [DataMember(Name = "date", Order = 2)]
        public DateTime DateCreated { set; get; }
    }
}
